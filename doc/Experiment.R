pdf("failRate0.1.pdf")
x = c(0.01, 0.02, 0.03, 0.04, 0.05)
y1 = c(29.1, 29.4, 32.2, 43.8, 73.5)
y2 = c(25.3, 25.9, 29.5, 41.4, 69.0)
matplot(x, data.frame(y1, y2), type="b",
    xlab = "Injection rate (flits/node/cycle)",
    ylab = "Average flit latency (cycles)")
legend("topleft", legend = c("Maze-routing", "Twist-routing"),
    col=1:4, pch="12")
dev.off()

pdf("failRate0.3.pdf")
x = c(0.003, 0.006, 0.009, 0.012, 0.015)
y1 = c(78.1, 79.3, 83.0, 102.9, 136.9)
y2 = c(50.5, 53.2, 57.9, 73.2, 101.6)
matplot(x, data.frame(y1, y2), type = "b",
    xlab = "Injection rate (flits/node/cycle)",
    ylab = "Average flit latency (cycles)",
    col=1:4, pch="12")
legend("topleft", legend = c("Maze-routing", "Twist-routing"),
    col=1:4, pch="12")
dev.off()
