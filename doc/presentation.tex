%% LyX 2.1.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[12pt]{beamer}
\usepackage[T1]{fontenc}
\setcounter{secnumdepth}{1}
\setcounter{tocdepth}{3}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\usepackage{graphicx}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
\pdfpageheight\paperheight
\pdfpagewidth\paperwidth

%% A simple dot to overcome graphicx limitations
\newcommand{\lyxdot}{.}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
 % this default might be overridden by plain title style
 \newcommand\makebeamertitle{\frame{\maketitle}}%
 % (ERT) argument for the TOC
 \AtBeginDocument{%
   \let\origtableofcontents=\tableofcontents
   \def\tableofcontents{\@ifnextchar[{\origtableofcontents}{\gobbletableofcontents}}
   \def\gobbletableofcontents#1{\origtableofcontents}
 }
\numberwithin{equation}{section}
\numberwithin{figure}{section}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage[no-math]{fontspec}

\usepackage{xeCJK}
\setCJKsansfont[BoldFont=FZLanTingHei-B-GBK, ItalicFont=FZLanTingHei-R-GBK, BoldItalicFont=FZLanTingHei-B-GBK]{FZLanTingHei-R-GBK}
\setCJKmainfont[BoldFont=FZLanTingHei-R-GBK, ItalicFont=FZLanTingSong, BoldItalicFont=FZLanTingHei-R-GBK]{FZLanTingSong}
\setCJKmonofont{FZLanTingSong}

\usetheme{metropolis}
\usefonttheme{professionalfonts}
\usepackage{eulervm}

\makeatother

\begin{document}

\title{Proposal: Some Improvements on Maze-routing Algorithm for Faulty
Network-On-Chips}


\author{Kunwei Zhang, Thomas Moscibroda}


\institute{Tsinghua University}
\makebeamertitle
\begin{frame}{Introduction}

\begin{itemize}
\item The transistor technology scales in microprocessors
\item More and more power-efficient cores on a single chip
\item The communication should be efficient 
\item Networks-on-chips (NoCs), instead of simple buses
\end{itemize}

\begin{center}
\includegraphics[width=1\textwidth]{noc1}
\par\end{center}


\end{frame}

\begin{frame}{Introduction (cont.)}

\begin{itemize}
\item The reliability of the on-chip components is reduced ...

\begin{itemize}
\item as critical dimensions shrink;
\item as the silicon ages.
\end{itemize}
\item Some failures should not cause an entire chip to fail. 
\end{itemize}
\end{frame}

\begin{frame}{The Model}

\begin{itemize}
\item A mesh of routers
\item Routers are placed on each grid points
\item Links are available between adjacent routers
\end{itemize}

\begin{center}
\includegraphics{noc2}
\par\end{center}

\end{frame}

\begin{frame}{The Model (cont.)}

\begin{itemize}
\item Each routers can be good or bad
\item Each undirected links can be healthy or faulty
\item A bad router is modeled by disabling all of its four links. 
\end{itemize}
\end{frame}

\begin{frame}{The Model (cont.)}

\begin{itemize}
\item Each node can send packets to any node, packets are ...

\begin{itemize}
\item splited into flits at the source node;
\item routed to the destination;
\item assembled from all flits at the destination.
\end{itemize}
\item In the routing algorithm, each router ...

\begin{itemize}
\item accepts input flits from all nearby healthy links;
\item permute them according to some rules;
\item send them back to all nearby healthy links.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Previous Works}

\begin{itemize}
\item Buffered: Ariadne (2011), uDirec (2013), Hermes (2014)

\begin{itemize}
\item Based on routing table
\item Incur reconfiguration overhead
\item Significant energy consumption caused by buffer usage
\end{itemize}
\item Deflection (for non-faulty networks):

\begin{itemize}
\item BLESS (2009), CHIPPER (2011), minBD (2012)
\end{itemize}
\item Deflection (for faulty networks):

\begin{itemize}
\item \textbf{Maze-routing} (2015)
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Maze-routing --- the Header}

\begin{itemize}
\item $src$, the source; 
\item $dst$, the destination; 
\item $md_{best}$, the closest Manhattan distance to $dst$ that the packet
has reached so far assuming a fault-free mesh; 
\item $mode$, the transmission mode:

\begin{itemize}
\item $greedy$;
\item \textrm{$\Rsh$}, clockwisely face-routing;
\item \textrm{$\Lsh$, }counter-clockwisely face-routing;
\end{itemize}
\item $n_{trav}$ and $dir_{trav}$, the node and direction which indicates
the destination is unreachable if it is visited again.
\end{itemize}
\end{frame}

\begin{frame}{Maze-routing --- the Algorithm}

\begin{itemize}
\item Each flit is routed to a productive output if possible. \\
This is called the $greedy$ mode. 
\item If there is no such output, the flit changes itself into face-routing
mode (randomly chosen from $\Rsh$ and $\Lsh$). 

\begin{itemize}
\item In face-routing mode $\Rsh$, the flit takes the first healthy output
on the left of the ray from $cur$ to $dst$, and then goes clockwisely. 
\item Effectively, the flit traverses the face underlying the ray from $cur$
to $dst$. 
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Maze-routing --- the Face-routing Mode}

\begin{itemize}
\item The flit changes back to $greedy$ mode when it goes to a router that
can forward it closer to its destination than the node where it entered
 face-routing mode, i.e., the $md_{best}$ in header can be reduced
by a neighbor link. 
\item If the $md_{best}$ cannot decrease until the flit has traversed the
whole face, which is detected by revisiting $n_{trav}$ on the direction
of $dir_{trav}$, then there is no path between $src$ and $dst$. 
\end{itemize}
\end{frame}

\begin{frame}{Maze-routing --- an Example}


\includegraphics[width=1\textwidth]{maze}
\end{frame}

\begin{frame}{Our Algorithm --- Maze{*}-routing}

\begin{itemize}
\item Based on Maze-routing algorithm
\item Use \textbf{bounding circles} to limit the search range
\item Theoretically bounded by the cube of optimal length
\item Asynchronously disable all isolated nodes
\end{itemize}
\end{frame}

\begin{frame}{Maze{*}-routing --- the Use of Bounding Circles}


When a flit enters  face-routing mode, draw a circle ...
\begin{itemize}
\item centered at the destination;
\item with the radius $c=\alpha_{0}\cdot md_{cur,dst}$.
\end{itemize}

If the flit is going to cross the boundary of the circle, ...
\begin{itemize}
\item Reverse the direction of the flit ($\Rsh\longleftrightarrow\Lsh$);
\item Enlarge the bounding circle (times it by $\alpha$);
\item Set $n_{trav}$ to the node where the reverse happens;
\item Set $dir_{trav}$ to the direction after being reversed.
\end{itemize}

Informally, the flit goes in a zig-zag way in face-routing mode.

\end{frame}

\begin{frame}{Maze{*}-routing --- After a Flit is Reversed}


After a reversion, the flit finds all its way back and goes on. There
are three cases for this flit, 
\begin{enumerate}
\item It is going to cross the other border of the bounding circle. The
bounding circle is enlarged again, and the direction of the flit is
reverse again and so on.
\item Its mode changes back to $greedy$, and $md_{best}$ decreases successfully.
\item It appears at the $n_{trav}$ again with the direction $dir_{trav}$.
This indicates that there is no path from $src$ to $dst$.
\end{enumerate}
\end{frame}

\begin{frame}{a Bad Case for Maze-routing}

\begin{columns}


\column{.6\textwidth}


\includegraphics[width=1\textwidth]{Drawing}


\column{.4\textwidth}


Maze-routing: 
\[
n+7\ \text{hops}
\]



Maze{*}-routing: 
\[
6\ \text{hops}
\]


\end{columns}

\begin{itemize}
\item Maze-routing needs $\Omega(n)$ hops.
\item It cannot be written as a function of the optimal length. 
\end{itemize}
\end{frame}

\begin{frame}{Maze{*}-routing is $O(m^{3})$}

\begin{itemize}
\item Let $m$ be the length of the optimal path.
\item Maze{*}-routing runs in $O(m^{3})$ hops.
\end{itemize}

\textbf{Theorem 1.} If the destination of a flit is reachable from
the source, and $m$ is the length of the optimal path of this flit,
the radius of the largest bounding circle used by Maze{*}-routing
without deflection is no more than $\max(\alpha^{2}m,c_{0})$, where
$c_{0}$ is the initial radius of the bounding circle.


\textbf{Theorem 2.} If the destination of a flit is reachable from
the source, and $m$ is the length of the optimal path of this flit,
Maze{*}-Routing can find a path with length $O(m^{3})$ for this flit
without deflection.


\begin{center}
\textbf{(Proofs are lengthy and omitted)}
\par\end{center}

\end{frame}

\begin{frame}{When the Chip Falls Apart}

\begin{itemize}
\item The network can be partitioned by faulty links. 
\item The Maze-routing algorithm can detect such cases, by noticing some
flits cannot reach there destination after a round trip around the
faces. 
\item However, there are disadvantages to rely on this method.

\begin{itemize}
\item It is wasteful to maintain the power of isolated nodes.
\item It causes heavy congestion at the face borders.
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{When the Chip Falls Apart (cont.)}

\begin{itemize}
\item We utilize a centralized processor. 
\item This processor maintains a list of all active nodes.
\item This processor may cut off the power supply of any nodes. 
\item When a node is going to send a flit, it checkes if the destination
is active with the centralized processor first. 
\end{itemize}
\end{frame}

\begin{frame}{When the Chip Falls Apart (cont.)}


When Maze{*}-routing algorithm reports that a flit cannot go to its
destination,
\begin{itemize}
\item this processor runs two floodfills independently of the network;

\begin{itemize}
\item one is started from $src$;
\item the other is started from $dst$. 
\end{itemize}
\item The network keeps functioning. 
\item Then, this processor compares the size of the $src$ part and the
$dst$ part in the network, and remove the smaller part from the network. 
\end{itemize}
\end{frame}

\begin{frame}{Simulation}


$32\times32$, Erdös--Rényi, failure rate $0.1$ or $0.3$, $1000$
cycles.


\hfill{}\includegraphics[width=0.44\textwidth]{/home/kunwei/GOAFRoute/doc/failRate0\lyxdot 1}\hfill{}\includegraphics[width=0.44\textwidth]{/home/kunwei/GOAFRoute/doc/failRate0\lyxdot 3}\hfill{}

\end{frame}

\begin{frame}{Thank you!}


\begin{center}
{\Huge{}Questions \& Answers}
\par\end{center}{\Huge \par}

\end{frame}

\end{document}
