case class Mesh(
    k:Int, ejEnd:Int, ejRate:Double,
    failRate:Double, seed:Int) {
    val rand = new util.Random(seed)
    val r = Array.fill(k, k){new Router}
    val bad = Flit(
        null, null, -1, null, -1, -1, -1, -1)
    var time = 0
    var flits, flitsUn, duration = 0L
    var finish = false

    for (i <- 0 until k) {
        r(k - 1)(i).inn(0) = bad
        r(k - 1)(i).out(0) = bad
        r(i)(k - 1).inn(1) = bad
        r(i)(k - 1).out(1) = bad
        r(0)(i).inn(2) = bad
        r(0)(i).out(2) = bad
        r(i)(0).inn(3) = bad
        r(i)(0).out(3) = bad
    }

    for (i <- 0 to k; j <- 0 until k)
        if (rand.nextDouble < failRate) {
            if (i > 0) {
                r(i - 1)(j).inn(0) = bad
                r(i - 1)(j).out(0) = bad
            }
            if (i < k) {
                r(i)(j).inn(2) = bad
                r(i)(j).out(2) = bad
            }
        }

    for (i <- 0 until k; j <- 0 to k)
        if (rand.nextDouble < failRate) {
            if (j > 0) {
                r(i)(j - 1).inn(1) = bad
                r(i)(j - 1).out(1) = bad
            }
            if (j < k) {
                r(i)(j).inn(3) = bad
                r(i)(j).out(3) = bad
            }
        }

    object RoutingMethod extends Enumeration {
        val Maze, Twist = Value
    }

    var m:RoutingMethod.Value = null

    def tick(m:RoutingMethod.Value) {
        this.m = m
        while (!finish) {
            finish = true
            if (time < ejEnd) finish = false
            for (i <- 0 until k; j <- 0 until k)
                r(i)(j).input(this, i, j)
            for (i <- 0 until k; j <- 0 until k)
                r(i)(j).route(this, i, j)
            for (i <- 0 until k; j <- 0 until k)
                r(i)(j).output(this, i, j)
            time += 1
        }
    }
}
