object Flit {
    def md(src:(Int, Int), tgt:(Int, Int)) = {
        Math.abs(src._1 - tgt._1) +
        Math.abs(src._2 - tgt._2)
    }
}

case class Flit(
    source:(Int, Int),
    target:(Int, Int),
    var dirTrav:Int,
    var nTrav:(Int, Int),
    var mdBest:Int,
    var mode:Int,
    var c:Int,
    begin:Int)
