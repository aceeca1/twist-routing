import collection._

class Router {
    var inn = Array.fill[Flit](4){null}
    var out = Array.fill[Flit](4){null}
    var o1, o2:Flit = null
    val in = mutable.Queue[Flit]()
    val b = mutable.Queue[Flit]()

    def input(mesh:Mesh, x:Int, y:Int) {
        if (x < mesh.k - 1)
            inn(0) = mesh.r(x + 1)(y).out(2)
        if (y < mesh.k - 1)
            inn(1) = mesh.r(x)(y + 1).out(3)
        if (x > 0)
            inn(2) = mesh.r(x - 1)(y).out(0)
        if (y > 0)
            inn(3) = mesh.r(x)(y - 1).out(1)
        def valid(fl:Flit) =
            !fl.eq(mesh.bad) && fl != null
        for (i <- 0 until 4)
            if (valid(inn(i)))
                mesh.finish = false
        if (in.nonEmpty || b.nonEmpty)
            mesh.finish = false
        if (0.until(4).forall{
            i => inn(i).eq(mesh.bad)}) return
        if (mesh.time < mesh.ejEnd &&
            mesh.rand.nextDouble < mesh.ejRate) {
            val source = (x, y)
            val tgtX = mesh.rand.nextInt(mesh.k)
            val tgtY = mesh.rand.nextInt(mesh.k)
            val target = (tgtX, tgtY)
            val dirTrav = -1
            val nTrav = null
            val mdBest = Flit.md(source, target)
            val mode = 0
            val c = -1
            val begin = mesh.time
            in.enqueue(Flit(
                source, target, dirTrav, nTrav,
                mdBest, mode, c, begin))
        }
    }

    def output(mesh:Mesh, x:Int, y:Int) {
        def stat(out:Flit) {
            mesh.flits += 1
            mesh.duration += mesh.time - out.begin
        }
        if (o1 != null) stat(o1)
        if (o2 != null) stat(o2)
    }

    def route(mesh:Mesh, x:Int, y:Int) {
        if (b.nonEmpty || in.nonEmpty) {
            val free = inn.indexOf(null)
            if (free != -1)
                inn(free) = if (b.nonEmpty)
                    b.dequeue
                else
                    in.dequeue
        }
        for (i <- 0 until 4)
            if (!out(i).eq(mesh.bad)) out(i) = null
        o1 = null
        o2 = null
        var e = mutable.Buffer[Int]()
        for (i <- 0 until 4)
            if (inn(i) != null &&
                !inn(i).eq(mesh.bad)) e += i
        e = e.sortBy{
            i => (inn(i).begin, inn(i).source)}
        def arrangeMaze(ei:Int):
            mutable.Buffer[Int] = {
            val (tgtX, tgtY) = inn(ei).target
            if (tgtX == x && tgtY == y)
                return mutable.Buffer(-1)
            if (inn(ei).mdBest ==
                Flit.md((x, y), inn(ei).target)) {
                val hP = mutable.Buffer[Int]()
                if (tgtX > x &&
                    !out(0).eq(mesh.bad)) hP += 0
                if (tgtY > y &&
                    !out(1).eq(mesh.bad)) hP += 1
                if (tgtX < x &&
                    !out(2).eq(mesh.bad)) hP += 2
                if (tgtY < y &&
                    !out(3).eq(mesh.bad)) hP += 3
                if (hP.nonEmpty) {
                    inn(ei).mdBest -= 1
                    inn(ei).mode = 0
                    return hP
                }
            }
            if (inn(ei).mode > 0) {
                val a = inn(ei).mode match {
                    case 1 => Array(
                        (ei + 3) & 3, (ei + 2) & 3,
                        (ei + 1) & 3, ei)
                    case 2 => Array(
                        (ei + 1) & 3, (ei + 2) & 3,
                        (ei + 3) & 3, ei)
                }
                val d = a.find{
                    ai => !out(ai).eq(mesh.bad)}.get
                if ((x, y) == inn(ei).nTrav &&
                    d == inn(ei).dirTrav) return null
                return mutable.Buffer(d)
            }
            inn(ei).mode = util.Random.nextInt(2) + 1
            var a = if (tgtX > x) {
                if (tgtY > y) Array(3, 2)
                else if (tgtY == y) Array(3, 2, 1)
                else Array(2, 1)
            } else if (tgtX == x) {
                if (tgtY > y) Array(0, 3, 2)
                else Array(2, 1, 0)
            } else {
                if (tgtY > y) Array(0, 3)
                else if (tgtY == y) Array(1, 0, 3)
                else Array(1, 0)
            }
            if (inn(ei).mode == 2) a = a.reverse
            val d = a.find{
                ai => !out(ai).eq(mesh.bad)}.get
            inn(ei).dirTrav = d
            inn(ei).nTrav = (x, y)
            return mutable.Buffer(d)
        }
        def arrangeTwist(ei:Int):mutable.Buffer[Int] = {
            def next(d:Int) = d match {
                case 0 => (x + 1, y)
                case 1 => (x, y + 1)
                case 2 => (x - 1, y)
                case 3 => (x, y - 1)
            }
            val (tgtX, tgtY) = inn(ei).target
            if (tgtX == x && tgtY == y)
                return mutable.Buffer(-1)
            if (inn(ei).mdBest ==
                Flit.md((x, y), inn(ei).target)) {
                val hP = mutable.Buffer[Int]()
                if (tgtX > x &&
                    !out(0).eq(mesh.bad)) hP += 0
                if (tgtY > y &&
                    !out(1).eq(mesh.bad)) hP += 1
                if (tgtX < x &&
                    !out(2).eq(mesh.bad)) hP += 2
                if (tgtY < y &&
                    !out(3).eq(mesh.bad)) hP += 3
                if (hP.nonEmpty) {
                    inn(ei).mdBest -= 1
                    inn(ei).mode = 0
                    return hP
                }
            }
            if (inn(ei).mode > 0) {
                val a = inn(ei).mode match {
                    case 1 => Array(
                        (ei + 3) & 3, (ei + 2) & 3,
                        (ei + 1) & 3, ei)
                    case 2 => Array(
                        (ei + 1) & 3, (ei + 2) & 3,
                        (ei + 3) & 3, ei)
                }
                val d = a.find{
                    ai => !out(ai).eq(mesh.bad)}.get
                if ((x, y) == inn(ei).nTrav &&
                    d == inn(ei).dirTrav) return null
                val nextD = next(d)
                if (Flit.md(nextD,
                    inn(ei).target) > inn(ei).c) {
                    inn(ei).c = (inn(ei).c << 2) + 1
                    inn(ei).mode = 3 - inn(ei).mode
                    inn(ei).dirTrav = ei
                    inn(ei).nTrav = (x, y)
                    return mutable.Buffer(ei)
                }
                return mutable.Buffer(d)
            }
            inn(ei).mode = util.Random.nextInt(2) + 1
            var a = if (tgtX > x) {
                if (tgtY > y) Array(3, 2)
                else if (tgtY == y) Array(3, 2, 1)
                else Array(2, 1)
            } else if (tgtX == x) {
                if (tgtY > y) Array(0, 3, 2)
                else Array(2, 1, 0)
            } else {
                if (tgtY > y) Array(0, 3)
                else if (tgtY == y) Array(1, 0, 3)
                else Array(1, 0)
            }
            if (inn(ei).mode == 2) a = a.reverse
            val d = a.find{
                ai => !out(ai).eq(mesh.bad)}.get
            inn(ei).dirTrav = d
            inn(ei).nTrav = (x, y)
            val nextD = next(d)
            inn(ei).c = (Flit.md(nextD,
                inn(ei).target) * 3 >> 1) + 1
            return mutable.Buffer(d)
        }
        val deflect = mutable.Buffer[Int]()
        def process(ei:Int) {
            val d = mesh.m match {
                case mesh.RoutingMethod.Maze
                    => arrangeMaze(ei)
                case mesh.RoutingMethod.Twist
                    => arrangeTwist(ei)
            }
            if (d == null) {
                mesh.flitsUn += 1
                return
            }
            if (d.head == -1) {
                if (o1 == null) {
                    o1 = inn(ei)
                    return
                }
                if (o2 == null) {
                    o2 = inn(ei)
                    return
                }
            } else for (di <- d)
                if (out(di) == null) {
                    out(di) = inn(ei)
                    return
                }
            deflect += ei
        }
        e.foreach(process)
        if (b.size < 4 && deflect.nonEmpty) {
            val fl = inn(deflect.remove(0))
            fl.mdBest = Flit.md((x, y), fl.target)
            fl.mode = 0
            b.enqueue(fl)
        }
        def goDeflect(ei:Int) {
            for (i <- 0 until 4)
                if (out(i) == null) {
                    val next = i match {
                        case 0 => (x + 1, y)
                        case 1 => (x, y + 1)
                        case 2 => (x - 1, y)
                        case 3 => (x, y - 1)
                    }
                    inn(ei).mdBest =
                        Flit.md(next, inn(ei).target)
                    inn(ei).mode = 0
                    out(i) = inn(ei)
                    return
                }
        }
        for (ei <- deflect) goDeflect(ei)
    }
}
