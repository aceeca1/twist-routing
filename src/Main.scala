object Main {
    import io.StdIn._

    def main(args:Array[String]) {
        val k = 32
        val ejEnd = 1000
        val ejRate = 0.05
        val failRate = 0.3
        var flits1, flits2, du1, du2 = 0L
        for (i <- 0 until 5) {
            println("Test " + i)
            val mesh1 = Mesh(
                k, ejEnd, ejRate, failRate, i)
            val mesh2 = Mesh(
                k, ejEnd, ejRate, failRate, i)
            mesh1.tick(mesh1.RoutingMethod.Maze)
            flits1 += mesh1.flits
            du1 += mesh1.duration
            mesh2.tick(mesh2.RoutingMethod.Twist)
            flits2 += mesh2.flits
            du2 += mesh2.duration
        }
        println(du1.toDouble / flits1)
        println(du2.toDouble / flits2)
    }
}
